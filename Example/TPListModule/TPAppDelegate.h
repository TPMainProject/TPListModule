//
//  TPAppDelegate.h
//  TPListModule
//
//  Created by TPQuietBro on 07/18/2017.
//  Copyright (c) 2017 TPQuietBro. All rights reserved.
//

@import UIKit;

@interface TPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
