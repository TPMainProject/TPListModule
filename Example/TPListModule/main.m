//
//  main.m
//  TPListModule
//
//  Created by TPQuietBro on 07/18/2017.
//  Copyright (c) 2017 TPQuietBro. All rights reserved.
//

@import UIKit;
#import "TPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TPAppDelegate class]));
    }
}
